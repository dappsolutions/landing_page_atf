

<?php if (!empty($data)) { ?>
 <?php foreach ($data as $value) { ?>
  <div class="entry clearfix">
   <div class="entry-image">
    <a href="<?php echo base_url().'files/berkas/announcement/'.$value['file'] ?>" data-lightbox="image"><img class="image_fade" src="<?php echo base_url().'files/berkas/announcement/'.$value['file'] ?>" alt="<?php echo $value['title'] ?>"></a>
   </div>
   <div class="entry-c">
    <div class="entry-title">
     <h2><a href="#"><?php echo $value['title'] ?></a></h2>
    </div>
    <ul class="entry-meta clearfix">
     <li><i class="icon-calendar3"></i> <?php echo date('d F Y', strtotime($value['tanggal'])) ?></li>
     <li><a href="#"><i class="icon-user"></i> <?php echo $value['username'] ?></a></li>
    </ul>
    <div class="entry-content">
     <p><?php echo $value['keterangan'] ?></p>
     <!--<a href="blog-single.html"class="more-link">Read More</a>-->
    </div>
   </div>
  </div>
 <?php } ?>
<?php } ?>