<div class="row">
 <div class="col-lg-3 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-aqua">
   <div class="inner">
    <h3><?php echo $total_user ?></h3>

    <p>User</p>
   </div>
   <div class="icon">
    <i class="ion ion-person"></i>
   </div>
   <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
  </div>
 </div>
 <!-- ./col -->
 <div class="col-lg-3 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-green">
   <div class="inner">
    <h3>53</h3>

    <p>File</p>
   </div>
   <div class="icon">
    <i class="ion ion-stats-bars"></i>
   </div>
   <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
  </div>
 </div>
 <!-- ./col -->
 <div class="col-lg-3 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-yellow">
   <div class="inner">
    <h3>44</h3>

    <p>Image</p>
   </div>
   <div class="icon">
    <i class="ion ion-android-menu"></i>
   </div>
   <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
  </div>
 </div>
 <!-- ./col -->
 <div class="col-lg-3 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-red">
   <div class="inner">
    <h3>65</h3>

    <p>Pesan</p>
   </div>
   <div class="icon">
    <i class="ion ion-pie-graph"></i>
   </div>
   <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
  </div>
 </div>
 <!-- ./col -->
</div>