<?php

class Gallery extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
 }

 public function getModuleName() {
  return 'gallery';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/gallery.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'gallery';
 }

 public function getRootModule() {
  return "About";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Gallery";
  $data['title_content'] = 'Gallery';
  $content = $this->getDataGallery();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataGallery($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.keterangan', $keyword),
       array('gc.kategori', $keyword),
   );
  }

  $where = "t.deleted = 0";
  if ($this->akses == 'Superadmin') {
   $where = "t.deleted = 0";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*', 'gc.kategori'),
                'join' => array(
                    array('gallery_category gc', 'gc.id = t.gallery_category')
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*', 'gc.kategori'),
                'join' => array(
                    array('gallery_category gc', 'gc.id = t.gallery_category')
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataGallery($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.keterangan', $keyword),
       array('gc.kategori', $keyword),
   );
  }

  $where = "t.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "t.deleted = 0";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*', 'gc.kategori'),
                'join' => array(
                    array('gallery_category gc', 'gc.id = t.gallery_category')
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*', 'gc.kategori'),
                'join' => array(
                    array('gallery_category gc', 'gc.id = t.gallery_category')
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataGallery($keyword)
  );
 }

 public function getDetailDataGallery($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListKategori() {
  $data = Modules::run('database/get', array(
              'table' => 'gallery_category',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Gallery";
  $data['title_content'] = 'Tambah Gallery';
  $data['list_kategori'] = $this->getListKategori();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataGallery($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Gallery";
  $data['title_content'] = 'Ubah Gallery';
  $data['list_kategori'] = $this->getListKategori();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataGallery($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Gallery";
  $data['title_content'] = "Detail Gallery";
  $data['list_kategori'] = $this->getListKategori();
  echo Modules::run('template', $data);
 }

 public function getPostDataGallery($value) {
  $data['gallery_category'] = $value->kategori;
  $data['keterangan'] = $value->keterangan;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $is_save = true;
  $file = $_FILES;
  $message = "";

//  echo '<pre>';
//  print_r($file);die;

  $this->db->trans_begin();
  try {
   $post = $this->getPostDataGallery($data->form);
   if ($id == '') {
    if (!empty($file)) {
     for ($i = 0; $i < count($file); $i++) {
      $response_upload = $this->uploadData('file-' . $i);
      if ($response_upload['is_valid']) {
       $post['file'] = $file['file-' . $i]['name'];
      } else {
       $is_save = false;
       $message = $response_upload['response'];
      }

      if ($is_save) {
       $id = Modules::run('database/_insert', $this->getTableName(), $post);
      }
     }
    } else {
     $id = Modules::run('database/_insert', $this->getTableName(), $post);
    }
   } else {
    //update
    if (!empty($file)) {
     for ($i = 0; $i < count($file); $i++) {
      if ($data->form->file_str != $file['file-' . $i]['name']) {
       $response_upload = $this->uploadData('file-' . $i);
       if ($response_upload['is_valid']) {
        $post['file'] = $file['file-' . $i]['name'];
       } else {
        $is_save = false;
        $message = $response_upload['response'];
       }
      }

      if ($is_save) {
       Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
      }
     }
    } else {
     if ($is_save) {
      Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
     }
    }
   }
   $this->db->trans_commit();
   if ($is_save) {
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Gallery";
  $data['title_content'] = 'Data Gallery';
  $content = $this->getDataGallery($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/gallery/';
  $config['allowed_types'] = 'png|jpg|jpeg';
  $config['max_size'] = '10000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $is_valid = false;
  if (!$this->upload->do_upload($name_of_field)) {
   $response = $this->upload->display_errors();
  } else {
   $response = $this->upload->data();
   $is_valid = true;
  }

  return array(
      'is_valid' => $is_valid,
      'response' => $response
  );
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data_foto = explode('.', $foto);
  $tipe_file = $data_foto[count($data_foto) - 1];

  if (!empty($data_foto)) {
   $foto = implode('_', $data_foto);
   $foto = str_replace("_" . $tipe_file, '.' . $tipe_file, $foto);
  }

//  echo $foto;die;
  $data['foto'] = $foto;
//  echo '<pre>';
//  print_r($data);die;
  echo $this->load->view('foto', $data, true);
 }

 public function getDataWeb() {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'gc.kategori'),
              'join' => array(
                  array('gallery_category gc', 'gc.id = t.gallery_category')
              ),
              'where' => 't.deleted = 0'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $foto = str_replace(' ', '_', $value['file']);
    $data_foto = explode('.', $foto);
    $tipe_file = $data_foto[count($data_foto) - 1];

    if (!empty($data_foto)) {
     $foto = implode('_', $data_foto);
     $foto = str_replace("_" . $tipe_file, '.' . $tipe_file, $foto);
    }
    $value['file'] = $foto;
    array_push($result, $value);
   }
  }


  $data_content['data'] = $result;
  $data_content['data_kategori'] = $this->getListKategori();
  $data_view = $this->load->view('content_gallery', $data_content, true);
  $data_cat = $this->load->view('content_kategori', $data_content, true);
  echo json_encode(array('data' => $data_view, 'kategori' => $data_cat));
 }

}
