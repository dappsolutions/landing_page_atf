
<?php if (!empty($data)) { ?>
 <?php foreach ($data as $value) { ?>
  <article class="portfolio-item pf-category-<?php echo $value['gallery_category'] ?>">
   <div class="portfolio-image">
    <a href="portfolio-single.html">
     <img src="<?php echo base_url() . 'files/berkas/gallery/' . $value['file'] ?>" alt="Open Imagination">
    </a>
    <div class="portfolio-overlay">
     <a href="<?php echo base_url() . 'files/berkas/gallery/' . $value['file'] ?>" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
     <a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
    </div>
   </div>
   <div class="portfolio-desc">
    <h3><a href="#"><?php echo $value['keterangan'] ?></a></h3>
    <span><a href="#"><?php echo $value['kategori'] ?></a></span>
   </div>
  </article>
 <?php } ?>
<?php } ?>