
<div class="col-lg-6 clearfix bottommargin-sm">
 <a href="<?php echo $data[0]['url'] ?>" target="_blank" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;" data-animate="bounceIn">
  <i class="icon-facebook"></i>
  <i class="icon-facebook"></i>
 </a>
 <a href="<?php echo $data[0]['url'] ?>" target="_blank"><small style="display: block; margin-top: 3px;"><strong>Like us</strong><br>on Facebook</small></a>
</div>
<div class="col-lg-6 clearfix bottommargin-sm">
 <a href="<?php echo $data[1]['url'] ?>" target="_blank" class="social-icon si-dark si-colored si-flickr nobottommargin" style="margin-right: 10px;" data-animate="bounceIn" data-delay="200">
  <i class="icon-instagram"></i>
  <i class="icon-instagram"></i>
 </a>
 <a href="<?php echo $data[1]['url'] ?>" target="_blank"><small style="display: block; margin-top: 3px;"><strong>Follow us</strong><br>on Instagram</small></a>
</div>
<div class="col-lg-6 clearfix bottommargin-sm">
 <a href="<?php echo $data[2]['url'] ?>" target="_blank" class="social-icon si-dark si-colored si-twitter nobottommargin" style="margin-right: 10px;" data-animate="bounceIn" data-delay="400">
  <i class="icon-twitter"></i>
  <i class="icon-twitter"></i>
 </a>
 <a href="<?php echo $data[2]['url'] ?>" target="_blank"><small style="display: block; margin-top: 3px;"><strong>Follow us</strong><br>on Twitter</small></a>
</div>
<div class="col-lg-6 clearfix bottommargin-sm">
 <a href="<?php echo $data[3]['url'] ?>" class="social-icon si-dark si-colored si-youtube nobottommargin" style="margin-right: 10px;" data-animate="bounceIn" data-delay="600">
  <i class="icon-youtube"></i>
  <i class="icon-youtube"></i>
 </a>
 <a href="<?php echo $data[3]['url'] ?>"><small style="display: block; margin-top: 3px;"><strong>Subscribe us</strong><br>on Youtube</small></a>
</div>