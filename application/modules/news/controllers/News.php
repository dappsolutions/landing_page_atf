<?php

class News extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
 }

 public function getModuleName() {
  return 'news';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/tinymce/js/tinymce/tinymce.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/news.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'news';
 }

 public function getRootModule() {
  return "Latest News";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - News";
  $data['title_content'] = 'News';
  $content = $this->getDataNews();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataNews($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('su.file', $keyword),
       array('su.title', $keyword),
       array('su.keterangan', $keyword),
       array('su.tanggal', $keyword),
   );
  }

  $where = "su.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "su.deleted = 0";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' su',
                'field' => array('su.*'),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' su',
                'field' => array('su.*'),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataNews($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('su.file', $keyword),
       array('su.title', $keyword),
       array('su.keterangan', $keyword),
       array('su.tanggal', $keyword),
   );
  }

  $where = "su.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "su.deleted = 0";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' su',
                'field' => array('su.*'),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' su',
                'field' => array('su.*'),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataNews($keyword)
  );
 }

 public function getDetailDataNews($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'where' => "t.id = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $data = $data->row_array();
   $data['title_news'] = $data['title'];
   $data['file'] = str_replace(' ', '_', $data['file']);
   $result = $data;
  }
  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah News";
  $data['title_content'] = 'Tambah News';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataNews($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah News";
  $data['title_content'] = 'Ubah News';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataNews($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail News";
  $data['title_content'] = "Detail News";
  echo Modules::run('template', $data);
 }

 public function getPostDataNews($value) {
  $data['keterangan'] = $value->keterangan;
  $data['tanggal'] = $value->tanggal;
  $data['title'] = $value->title;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $is_save = true;
  $file = $_FILES;

  $message = "";
  $this->db->trans_begin();
  try {
   $post = $this->getPostDataNews($data->form);
   if ($id == '') {
    if (!empty($file)) {
     $response_upload = $this->uploadData('file');
     if ($response_upload['is_valid']) {
      $post['file'] = $file['file']['name'];
     } else {
      $is_save = false;
      $message = $response_upload['response'];
     }
    }
    if ($is_save) {
     $id = Modules::run('database/_insert', $this->getTableName(), $post);
    }
   } else {
    //update
    if (!empty($file)) {
     if ($data->form->file_str != $file['file']['name']) {
      $response_upload = $this->uploadData('file');
      if ($response_upload['is_valid']) {
       $post['file'] = $file['file']['name'];
      } else {
       $is_save = false;
       $message = $response_upload['response'];
      }
     }
    }
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   if ($is_save) {
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data News";
  $data['title_content'] = 'Data News';
  $content = $this->getDataNews($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/news/';
  $config['allowed_types'] = 'png|jpg|jpeg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);

  $is_valid = false;
  if (!$this->upload->do_upload($name_of_field)) {
   $response = $this->upload->display_errors();
  } else {
   $response = $this->upload->data();
   $is_valid = true;
  }

  return array(
      'is_valid' => $is_valid,
      'response' => $response
  );
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
//  echo '<pre>';
//  print_r($data);die;
  echo $this->load->view('foto', $data, true);
 }

 public function getDataWebLimit() {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'where' => 't.deleted = 0',
              'limit' => 4,
              'orderby' => 't.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['file'] = str_replace(' ', '_', $value['file']);
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;
  $data_content['data'] = $result;
  $view = $this->load->view('content', $data_content, true);
  echo json_encode(array('total' => count($result), 'view' => $view));
 }

 public function getDataWeb() {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'where' => 't.deleted = 0',
              'orderby' => 't.id desc, t.tanggal desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $month = date('m', strtotime($value['tanggal']));
    $year = date('Y', strtotime($value['tanggal']));
    $value['month'] = Modules::run('helper/getIndoMonhtDate', $month);
    $group_header_date = $value['month'] . ' ' . $year;
    $value['group_date'] = $group_header_date;
    
    $value['file'] = str_replace(' ', '_', $value['file']);
    array_push($result, $value);
   }
  }

  $fix_result = array();
  if (!empty($result)) {
   $temp = "";
   foreach ($result as $value) {
    $group_date = $value['group_date'];
    if ($temp != $group_date) {
     $detail = array();
     foreach ($result as $value_child) {
      $group_date_child = $value_child['group_date'];
      if ($group_date_child == $group_date) {
       array_push($detail, $value_child);
      }
     }

     $fix_result[$group_date]['detail'] = $detail;
     $temp = $group_date;
    }
   }
  }

//  echo '<pre>';
//  print_r($fix_result);
//  die;
  $data_content['data'] = $fix_result;
  $view = $this->load->view('content_all_news', $data_content, true);
  echo $view;
 }

 public function setSessionId($id) {
  $this->session->set_userdata(array(
      'news_id' => $id
  ));
 }

 public function getDetailNews() {
  $id = $this->session->userdata('news_id');
  $data = $this->getDetailDataNews($id);

  $data['relate'] = $this->getRelateNews();
  $view = $this->load->view('content_detail_news', $data, true);
  echo json_encode(array('view' => $view));
 }

 public function getRelateNews() {
  $id = $this->session->userdata('news_id');
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
             'where' => "t.deleted = 0",
              'orderby' => 't.id desc, t.tanggal desc'
  ));
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['file'] = str_replace(' ', '_', $value['file']);
    array_push($result, $value);
   }
  }


  return $result;
 }

}
