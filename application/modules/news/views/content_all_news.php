<?php if (!empty($data)) { ?>
 <?php foreach ($data as $key => $value) { ?>
  <?php foreach ($value['detail'] as $v_data) { ?>
   <div class="entry clearfix">
    <div class="entry-image">
     <a href="<?php echo base_url() . 'files/berkas/news/' . $v_data['file'] ?>" data-lightbox="image"><img class="image_fade" src="<?php echo base_url() . 'files/berkas/news/' . $v_data['file'] ?>" alt="<?php echo $v_data['title'] ?>"></a>
    </div>
    <div class="entry-c">
     <div class="entry-title">
      <h2><a href="#" data_id="<?php echo $v_data['id'] ?>" onclick="News.setDetail(this, event)"><?php echo $v_data['title'] ?></a></h2>
     </div>
     <ul class="entry-meta clearfix">
      <li><i class="icon-calendar3"></i> <?php echo date('d F Y', strtotime($v_data['tanggal'])) ?></li>
      <!--<li><a href="#"><i class="icon-user"></i> <?php echo $v_data['username'] ?></a></li>-->
     </ul>
     <div class="entry-content">
      <p><?php echo substr($v_data['keterangan'], 0, 400) ?>.....</p>
       <a href="#" data_id="<?php echo $v_data['id'] ?>" onclick="News.setDetail(this, event)" class="more-link">Read More</a>
     </div>
    </div>
   </div>
  <?php } ?>
 <?php } ?>
<?php } ?>
