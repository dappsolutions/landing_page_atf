<div class="container clearfix">

	<div class="single-post nobottommargin">

		<!-- Single Post
   ============================================= -->
		<div class="entry clearfix">

			<!-- Entry Title
    ============================================= -->
			<div class="entry-title">
				<h2><?php echo $title ?></h2>
			</div><!-- .entry-title end -->

			<!-- Entry Meta
    ============================================= -->
			<ul class="entry-meta clearfix">
				<li><i class="icon-calendar3"></i> <?php echo date('d M Y', strtotime($createddate)) ?></li>
				<li><a href="#"><i class="icon-user"></i> Admin</a></li>
				<li><i class="icon-folder-open"></i> <a href="#">General Category</a></li>
			</ul><!-- .entry-meta end -->

			<!-- Entry Image
    ============================================= -->
			<div class="entry-image bottommargin">
				<a href="#"><img src="<?php echo base_url() . 'files/berkas/news/' . $file ?>" alt="Blog Single"></a>
			</div><!-- .entry-image end -->

			<!-- Entry Content
    ============================================= -->
			<div class="entry-content notopmargin">

				<?php echo $keterangan ?>
				<!-- Post Single - Content End -->

				<div class="clear"></div>

			</div>

		</div><!-- .entry end -->

		<!-- Post Navigation
   ============================================= -->
		<!-- <div class="post-navigation clearfix">

   <div class="col_half nobottommargin">
    <a href="#">&lArr; This is a Standard post with a Slider Gallery</a>
   </div>

   <div class="col_half col_last tright nobottommargin">
    <a href="#">This is an Embedded Audio Post &rArr;</a>
   </div>

  </div> -->
		<!-- .post-navigation end -->

		<!-- <div class="line"></div> -->

		<!-- Post Author Info
   ============================================= -->
		<div class="card">
			<div class="card-header"><strong>Posted by <a href="#">Admin</a></strong></div>
			<div class="card-body">
				<div class="author-image">
					<img src="images/author/1.jpg" alt="" class="rounded-circle">
				</div>
				<!--    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores, eveniet, eligendi et nobis neque minus mollitia sit repudiandae ad repellendus recusandae blanditiis praesentium vitae ab sint earum voluptate velit beatae alias fugit accusantium laboriosam nisi reiciendis deleniti tenetur molestiae maxime id quaerat consequatur fugiat aliquam laborum nam aliquid. Consectetur, perferendis?-->
			</div>
		</div><!-- Post Single - Author End -->

		<div class="line"></div>

		<h4>Related Posts:</h4>

		<?php if (!empty($relate)) { ?>
			<div class="related-posts clearfix">

				<div class="col_half nobottommargin">

					<div class="mpost clearfix">
						<div class="entry-image">
							<a href="#" data_id="<?php echo $relate[0]['id'] ?>" onclick="News.setDetail(this, event)"><img src="<?php echo base_url() . 'files/berkas/news/' . $relate[0]['file'] ?>" alt="Blog Single"></a>
						</div>
						<div class="entry-c">
							<div class="entry-title">
								<h4><a href="#"><?php echo $relate[0]['title'] ?></a></h4>
							</div>
							<ul class="entry-meta clearfix">
								<li><i class="icon-calendar3"></i> <?php echo date('d M Y', strtotime($relate[0]['createddate'])) ?></li>
							</ul>
							<!--<div class="entry-content"><?php echo substr($relate[0]['keterangan'], 0, 80) ?></div>-->
						</div>
					</div>

					<?php if (count($relate) > 1) { ?>
						<div class="mpost clearfix">
							<div class="entry-image">
								<a href="#" data_id="<?php echo $relate[1]['id'] ?>" onclick="News.setDetail(this, event)"><img src="<?php echo base_url() . 'files/berkas/news/' . $relate[1]['file'] ?>" alt="Blog Single"></a>
							</div>
							<div class="entry-c">
								<div class="entry-title">
									<h4><a href="#"><?php echo $relate[1]['title'] ?></a></h4>
								</div>
								<ul class="entry-meta clearfix">
									<li><i class="icon-calendar3"></i> <?php echo date('d M Y', strtotime($relate[1]['createddate'])) ?></li>
								</ul>
							</div>
						</div>
					<?php } ?>
				</div>

				<div class="col_half nobottommargin col_last">
					<?php if (count($relate) > 2) { ?>
						<div class="mpost clearfix">
							<div class="entry-image">
								<a href="#" data_id="<?php echo $relate[2]['id'] ?>" onclick="News.setDetail(this, event)"><img src="<?php echo base_url() . 'files/berkas/news/' . $relate[2]['file'] ?>" alt="Blog Single"></a>
							</div>
							<div class="entry-c">
								<div class="entry-title">
									<h4><a href="#"><?php echo $relate[2]['title'] ?></a></h4>
								</div>
								<ul class="entry-meta clearfix">
									<li><i class="icon-calendar3"></i> <?php echo date('d M Y', strtotime($relate[2]['createddate'])) ?></li>
								</ul>
							</div>
						</div>
					<?php } ?>
					<?php if (count($relate) > 3) { ?>
						<div class="mpost clearfix">
							<div class="entry-image">
								<a href="#" data_id="<?php echo $relate[3]['id'] ?>" onclick="News.setDetail(this, event)"><img src="<?php echo base_url() . 'files/berkas/news/' . $relate[3]['file'] ?>" alt="Blog Single"></a>
							</div>
							<div class="entry-c">
								<div class="entry-title">
									<h4><a href="#"><?php echo $relate[3]['title'] ?></a></h4>
								</div>
								<ul class="entry-meta clearfix">
									<li><i class="icon-calendar3"></i> <?php echo date('d M Y', strtotime($relate[3]['createddate'])) ?></li>
								</ul>
							</div>
						</div>
					<?php } ?>

				</div>

			</div>
		<?php } ?>
	</div>

</div>
