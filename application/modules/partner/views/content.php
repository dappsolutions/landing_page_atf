

<?php if (!empty($data)) { ?>
 <?php foreach ($data as $value) { ?>
  <div class="oc-item" data-animate="fadeInLeft">
   <div class="team team-list clearfix">
    <div class="team-image">
     <img src="<?php echo base_url() . 'files/berkas/partner/' . $value['file'] ?>" alt="Image">
    </div>
    <div class="team-desc">
     <div class="team-title"><h4><?php echo $value['title'] ?></h4></div>
     <!-- <div class="team-content">
      <p>Some Text.</p>
     </div> -->
    </div>
   </div>
  </div>
 <?php } ?>
<?php } ?>