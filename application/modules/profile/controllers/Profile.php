<?php

class Profile extends MX_Controller {

 public function getFormChangePassword() {
  $data['username'] = $this->session->userdata('username');
  $data['title_content'] = 'Form Ganti Password';
  echo $this->load->view('form_change_password', $data, true);
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/profile.js"></script>'
  );

  return $data;
 }
 
 public function simpanPassword() {

  $username = $this->input->post('username');
  $password_lama = $this->input->post('password_lama');
  $password_baru = $this->input->post('password_baru');

  $user_id = $this->session->userdata('user_id');


  $is_valid = false;
  $message = "";
  $valid_user = $this->getDataUserIsValid($username, $password_lama);
  if ($valid_user) {
   //update password
   if ($password_baru == $password_lama) {
    $is_valid = false;
    $message = "Password Tidak Boleh Sama";
   } else {
    $password_baru = $password_baru;
    Modules::run('database/_update', 'user', array('password' => $password_baru), array('id' => $user_id));
    $is_valid = true;
   }
  } else {
   $message = "Password Tidak Valid";
  }

  echo json_encode(array('is_valid' => $is_valid, 'message' => $message));
 }

 public function getDataUserIsValid($username, $password) {
  $data = Modules::run('database/get', array(
              'table' => 'user',
              'where' => array('username' => $username, 'password' => $password)
  ));

  $is_valid = false;
  if (!empty($data)) {
   $is_valid = true;
  }

  return $is_valid;
 }

 public function getDetailDataProfil($id) {
  $data = Modules::run('database/get', array(
              'table' => 'profile p',
              'field' => array('p.*'),
              'where' => array('p.id' => $id)
  ));
  return $data->row_array();
 }

 public function ubah($id) {
  $data = $this->getDetailDataProfil($id);
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['view_file'] = 'form_add_edit_view';
  $data['module'] = "profile";
  $data['title'] = "Ubah Profile";
  $data['title_content'] = 'Ubah Profile';
  echo Modules::run('template', $data);
 }

 public function getPostDataProfile($value) {
  $data['alamat'] = $value->alamat;
  $data['no_hp'] = $value->no_hp;
  $data['email'] = $value->email;
  return $data;
 }
 
 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post = $this->getPostDataProfile($data->form);
   if ($id == '') {
    $id = Modules::run('database/_insert', 'profile', $post);
   } else {
    //update
    Modules::run('database/_update', "profile", $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function detailProfile(){
	$data = $this->getDetailDataProfil(1);
	echo json_encode($data);
 }
}
