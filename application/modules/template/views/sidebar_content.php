<aside class="main-sidebar">
 <!-- sidebar: style can be found in sidebar.less -->
 <section class="sidebar">
  <!-- Sidebar user panel -->
  <div class="user-panel">
   <div class="pull-left image">
    <img src="<?php echo base_url() ?>assets/admin_lte/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
   </div>
   <div class="pull-left info">
    <p><?php echo ucfirst($this->session->userdata('username')) ?></p>
    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
   </div>
  </div>
  <!-- /.search form -->
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu" data-widget="tree">
   <li class="header">MAIN NAVIGATION</li>
   <li><a href="<?php echo base_url() . 'dashboard' ?>"><i class="fa fa-folder-o"></i> <span>Dashboard</span></a></li>
   <li class="treeview">
    <a href="#">
     <i class="fa fa-folder"></i>
     <span>Home</span>
     <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
     </span>
    </a>
    <ul class="treeview-menu">
     <li><a href="<?php echo base_url() . 'slider' ?>"><i class="fa fa-file-text-o"></i> Slider</a></li>
    </ul>
   </li>   

   <li class="treeview">
    <a href="#">
     <i class="fa fa-folder"></i>
     <span>About</span>
     <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
     </span>
    </a>
    <ul class="treeview-menu">
     <li><a href="<?php echo base_url() . 'history' ?>"><i class="fa fa-file-text-o"></i> History</a></li>
     <li><a href="<?php echo base_url() . 'mission' ?>"><i class="fa fa-file-text-o"></i> Mission</a></li>
     <li><a href="<?php echo base_url() . 'wedo' ?>"><i class="fa fa-file-text-o"></i> Our Team</a></li>
     <li><a href="#"><hr/></a></li>
     <li><a href="<?php echo base_url() . 'event' ?>"><i class="fa fa-file-text-o"></i> Event</a></li>
     <li><a href="<?php echo base_url() . 'announcement' ?>"><i class="fa fa-file-text-o"></i> Anouncement</a></li>
     <li><a href="<?php echo base_url() . 'gallery' ?>"><i class="fa fa-file-text-o"></i> Gallery</a></li>
     <li><a href="<?php echo base_url() . 'gallery_category' ?>"><i class="fa fa-file-text-o"></i> Gallery Category</a></li>
    </ul>
   </li>   
  
   <li class="treeview">
    <a href="#">
     <i class="fa fa-folder"></i>
     <span>Latest News</span>
     <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
     </span>
    </a>
    <ul class="treeview-menu">
     <li><a href="<?php echo base_url() . 'news' ?>"><i class="fa fa-file-text-o"></i> News</a></li>
    </ul>
   </li>   
   
   <li class="treeview">
    <a href="#">
     <i class="fa fa-folder"></i>
     <span>Our Activity</span>
     <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
     </span>
    </a>
    <ul class="treeview-menu">
     <li><a href="<?php echo base_url() . 'partner' ?>"><i class="fa fa-file-text-o"></i> Activity</a></li>
    </ul>
   </li>   
   
   <li class="treeview">
    <a href="#">
     <i class="fa fa-folder"></i>
     <span>Footer</span>
     <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
     </span>
    </a>
    <ul class="treeview-menu">
     <li><a href="<?php echo base_url() . 'group' ?>"><i class="fa fa-file-text-o"></i> Our Partner</a></li>
     <li><a href="<?php echo base_url() . 'program' ?>"><i class="fa fa-file-text-o"></i> Public Information</a></li>
     <li><a href="<?php echo base_url() . 'media' ?>"><i class="fa fa-file-text-o"></i> Media Sosial</a></li>
     <li><a href="<?php echo base_url() . 'kontak' ?>"><i class="fa fa-file-text-o"></i> Kontak Wa</a></li>
    </ul>
   </li>   

   <li><a href="<?php echo base_url() . 'profile/ubah/1' ?>"><i class="fa fa-gear"></i> <span>Profile</span></a></li>   
   <li><a href="<?php echo base_url() . 'contact_us' ?>"><i class="fa fa-inbox"></i> <span>Contact Us</span></a></li>   
   <li><a href="<?php echo base_url() . 'login/sign_out' ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>   
  </ul>
 </section>
 <!-- /.sidebar -->
</aside>
