<div class="row">    
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box padding-16">
     <div class="box-header with-border">
      <h4 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo strtoupper($title) ?></h4                                                                                                                                                                                                                                                           >
      <div class="divider"></div>
     </div>
     <!-- /.box-header -->
     <!-- form start -->
     <div class="box-body">
      <div class="row">
       <div class="col-md-12">
        <div class="input-group">
         <input type="text" class="form-control" onkeyup="Vendor.search(this, event)" id="keyword" placeholder="Pencarian">
         <span class="input-group-addon"><i class="fa fa-search"></i></span>
        </div>
       </div>          
      </div>
      <div class="divider"></div>
      <br/>
      <?php if (isset($keyword)) { ?>
       <br/>
       <div class="row">
        <div class="col-md-6">
         Cari : <label class="bold"><b><?php echo $keyword ?></b></label>
        </div>
       </div>
      <?php } ?>      
      <div class="row">
       <div class="col-md-12">        
        <div class="table-responsive">
         <table class="table table-bordered" id="tb_content">
          <thead>
           <tr class="bg-primary-light text-white">
            <th>No</th>
            <th>Nama Vendor</th>
            <th>Pimpinan</th>
            <th>Email</th>
            <th>No HP</th>
            <th>Action</th>
           </tr>
          </thead>
          <tbody>
           <?php if (!empty($content)) { ?>
            <?php $no = $pagination['last_no'] + 1; ?>
            <?php foreach ($content as $value) { ?>
             <?php $tr_color = "bg-warning"; ?>
             <?php if ($value['status'] == 'APPROVED') { ?>
              <?php $tr_color = "bg-success"; ?>
             <?php } ?>
             <?php if ($value['status'] == 'REJECTED') { ?>
              <?php $tr_color = "bg-danger"; ?>
             <?php } ?>
             <tr class="<?php echo $tr_color ?>">
              <td><?php echo $no++ ?></td>
              <td><?php echo $value['nama_vendor'] ?></td>
              <td><?php echo $value['pimpinan'] ?></td>
              <td><?php echo $value['email'] ?></td>
              <td><?php echo $value['no_hp'] ?></td>
              <td class="text-center">
  <!--               <i class="fa fa-trash grey-text hover" onclick="Vendor.delete('<?php echo $value['id'] ?>')"></i>
               &nbsp;-->
  <!--               <i class="fa fa-pencil grey-text  hover" onclick="Vendor.ubah('<?php echo $value['id'] ?>')"></i>              
                 &nbsp;-->
               <i class="fa fa-file-text grey-text  hover" onclick="Vendor.detail('<?php echo $value['id'] ?>')"></i>
              </td>
             </tr>
            <?php } ?>
           <?php } else { ?>
            <tr>
             <td colspan="10" class="text-center">Tidak ada data ditemukan</td>
            </tr>
           <?php } ?>           
          </tbody>
         </table>
        </div>
       </div>
      </div>
     </div>

     <div class="box-footer clearfix">
      <ul class="pagination pagination-sm no-margin pull-right">
       <?php echo $pagination['links'] ?>
      </ul>
     </div>
    </div>    
   </div>
  </div>       
 </div>
</div>