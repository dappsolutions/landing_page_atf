<?php if (!empty($data)) { ?>
 <?php foreach ($data as $value) { ?>
  <div class="col-lg-4 mb-4" data-animate="bounceIn" data-delay="200">
   <div class="flip-card text-center">
    <div class="flip-card-front no-after" data-height-xl="300" style="background-image: url('images/our_team.png')">
    </div>
    <div class="flip-card-back bg-danger no-after" data-height-xl="300">
     <div class="flip-card-inner">
      <i class="icon-line2-emoticon-smile h1"></i>
      <br>
      <a href="#" class="btn btn-outline-light mt-2"><?php echo $value['keterangan'] ?></a>
     </div>
    </div>
   </div>
  </div>
 <?php } ?>
<?php } ?>