-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 05, 2019 at 05:55 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lp_atf`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `file` text,
  `keterangan` longtext,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`id`, `user`, `title`, `tanggal`, `file`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'Judul Coba', '2019-10-17', 'blank.png', 'tesk ok bos', '2019-10-17', 1, NULL, NULL, 0),
(2, 1, 'Judul 2', '2019-10-17', 'blank.png', '<p>Oke os event e<img title=\"frame.png\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAYAAAB5fY51AAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAZiS0dEAAAAAAAA+UO7fwAAAAlwSFlzAAAASAAAAEgARslrPgAABydJREFUeNrt3cGRo0AUREHsaP8ta0cYB3QQBzS/qvNFcN1oUJF7Iua6JSmkyyOQBCxJApYkYEkSsCQJWJKAJUnAkiRgSQKWJAFLkoAlCViSBCxJApYkYEkSsCQJWJKAJUnAkiRgSQKWJAFLkoAlCViSBCxJApYkYEkSsCQBS5KAJUnAkgQsSQLWV6217uu6XA+uRz/0gDO8dV47m7MzYLmABSxgAQtYwHIBC1jAAhawgOUCFrCABSxgAcsFLGABC1jAApYLWMACFrCABSwXsAwJWMACFrAMCVjAAtZ4sPbed2tPnkMaFmlg2RmwDAlYwLIzYBkSsOwMWMACFrDsDFiGBCxgAQtYhgQsOwMWsIAFLDsDliEBC1jAApYhAcvOgAUsYAHLzoBlSMACFrAOByvtW6y3hjThG8VmsOwMWIYELGABC1jAApadAQtYwAIWsIBlSMACFrCABSxg2RmwgAUsYAELWIYELGABC1jAApadAcuQgAUsYAHLkIAFLGABC1jl3yjaGbAMCVjAsjNgGRKw7AxYwAIWsOwMWIYELGABC1iGBCw7AxawgAUsOwOWIQELWMACliEBy86ABSxgAcvOgGVIwAIWsIBlSC+CNeE52BmwgAUsYNkZsAwJWMCyM2AZErDsDFjAAhaw7AxYhgQsYAELWIYELDsDFrCABSw7A5YhAQtYwAKWIQHLzoBlSMAClp0B69+HlFbqkBqfr50By5CAZWd2BixDkp0By5AMCVh2BixDApad2RmwDEl2BixDMiRg2RmwDAlYdmZnwDIk2RmwDMmQgGVnwDIkYNmZnRWC5ZrzZ77S/l07y/wGFFjAApYLWMACFrCABSwXsIAFLGABC1guYAELWMACFrBcwAIWsIAFLGC5gGVIwAIWsIBlSMACFrCABSxguYClkE4YvkK36REIWAKWgAUsAUvAErAkYAlYAhawBCwBS8ASsIAlYAlYErAELAFLwAKWgPXLlyTt5fPN35xnlrbftGcGLGABC1jAAhawgAUsYAELWMACFrCABSxgAQtYwAIWsIAFLGABC1jAAhawgAUsYAELWMACFrCABSxgAQtYwAIWsMb/4M3nbf6OD4T9vzGwgAUsZwAWsIAFLGABC1jAggWwgAUsYDkDsIAFLGABC1jAAhYsgAUsYAHLGYAFLGABC1jAApYxwwJYwAIWsJwBWON/mOZvvJyh/2r+Tx9YwHIGYAELWLAAlu0AC1jAAhawgAUsZwAWsIAFC2DZDrCABSxgAQtYRucMwAIWsGABLNsBFrCcAVjAApbROQOwgFWVb/76/6PxfLM3CSzjAJbnCyxgAQtYNgks4/DMgAUsGQewPF9gGYcXClg2CSzj8MyABSxgGQewPF9gGYcXClg2CSzjAJbnCyxgAQtYni+wxv7gzd+ZNb8k/hzXOcgDC1jAAhawgAUsYAELWMACFrCABSxgAQtYwAIWsIAFLGABC1jAAhawgAUsYAELWMACFrCABSxgAQtYwAIWsIAFLA2H272BcOxv7VUGFrCABSwBC1h+N2DJ8IEFLAELWMACloAFLL8bsGT4wAKWgOXegAUsAQtYfjdgCVjAApaABSxgAetTa63q7/78yaoZYMHNt4TAAhawgAUsF7CABSxgAQtYwAIWsIAFLGABywUsYAELWMACFrCABSxgAQtYwHIBC1jAAhawgAUsYAELWMAC1uFg7b1rX74nz6EZC/8hvAshsIAFLGABC1jAAhawgAUsYAELWMACFrCABSxgAQtYwAIWsIAFLGABC1jAAhawgAUsYAELWMACFrCABSxgAQtYwAJWyAuVBpbv7ZwBWMAClhfVGYAFLGA5A7CABSxgOQOwgAUsWDgDsIAFLGcAFrCABSxnABawgAULYAELWMByBmABC1jAcgZgAQtYsAAWsIAFoYOQP+FPxAELWMACFrCABSxgAQtYwAIWsIAFLGABC1jAAhawgAUsYAELWMACFrCABQtgAQtYwAIWsIAFLGABC1jAAhawgAUsYBWBBc3+ewMWsIDl3oAFLGB5qd0bsIAFLC81sIAFLGC5N2ABC1heavcGLGABy0sNLGABC1juDVjAApaX2r0BC1jA8lIDC1jAApZ7A9axYKU1Aazm7+J88wcsYAELWMACFrCABSwBC1jAAhawgAUsYAELWMACFrAELGABC1jAAhawgAUsYAELWMACFrCABSxgAQtYwAIWsA4Hy9X/ovrzYf3nBRawgAUsYAELWMACFrCABSxgAQtYwAIWsIAFLGABC1jAAhawgAUsYAELWMACFrCABSxgAcsFLGABC1jAAhawgCVJwJIkYEkCliQBS5KAJQlYkgQsScCSJGBJErAkAUuSgCVJwJIELEkCliQBSxKwJAlYkgQsScCSJGBJErAkAUuSgCVJwJIELEkCliQBSxKwJAlYkgQsScCSJGBJErAkhfUHrmEs1vEGqLYAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTktMTAtMzBUMDY6Mjg6NDQrMDA6MDB5tHHMAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE5LTEwLTMwVDA2OjI4OjQ0KzAwOjAwCOnJcAAAACh0RVh0c3ZnOmJhc2UtdXJpAGZpbGU6Ly8vdG1wL21hZ2ljay1mS2IzZXhVTDTceUQAAAAASUVORK5CYII=\" alt=\"\" width=\"100\" height=\"100\" /></p>', '2019-10-17', 1, '2019-11-04 19:02:36', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `email` text,
  `subject` varchar(150) DEFAULT NULL,
  `message` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `subject`, `message`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 'Dodik Rismawan Affrudin', 'solutionsdapps@gmail.com', 'asdad', 'asdad', '2019-11-04', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` longtext,
  `file` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `user`, `title`, `tanggal`, `keterangan`, `file`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'Event Judul', '2019-10-17', 'Oke bos event', 'blank.png', '2019-10-17', 1, NULL, NULL, 0),
(2, 1, 'Event Judul', '2019-11-05', '<p><img title=\"frame.png\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAYAAAB5fY51AAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAZiS0dEAAAAAAAA+UO7fwAAAAlwSFlzAAAASAAAAEgARslrPgAABydJREFUeNrt3cGRo0AUREHsaP8ta0cYB3QQBzS/qvNFcN1oUJF7Iua6JSmkyyOQBCxJApYkYEkSsCQJWJKAJUnAkiRgSQKWJAFLkoAlCViSBCxJApYkYEkSsCQJWJKAJUnAkiRgSQKWJAFLkoAlCViSBCxJApYkYEkSsCQBS5KAJUnAkgQsSQLWV6217uu6XA+uRz/0gDO8dV47m7MzYLmABSxgAQtYwHIBC1jAAhawgOUCFrCABSxgAcsFLGABC1jAApYLWMACFrCABSwXsAwJWMACFrAMCVjAAtZ4sPbed2tPnkMaFmlg2RmwDAlYwLIzYBkSsOwMWMACFrDsDFiGBCxgAQtYhgQsOwMWsIAFLDsDliEBC1jAApYhAcvOgAUsYAHLzoBlSMACFrAOByvtW6y3hjThG8VmsOwMWIYELGABC1jAApadAQtYwAIWsIBlSMACFrCABSxg2RmwgAUsYAELWIYELGABC1jAApadAcuQgAUsYAHLkIAFLGABC1jl3yjaGbAMCVjAsjNgGRKw7AxYwAIWsOwMWIYELGABC1iGBCw7AxawgAUsOwOWIQELWMACliEBy86ABSxgAcvOgGVIwAIWsIBlSC+CNeE52BmwgAUsYNkZsAwJWMCyM2AZErDsDFjAAhaw7AxYhgQsYAELWIYELDsDFrCABSw7A5YhAQtYwAKWIQHLzoBlSMAClp0B69+HlFbqkBqfr50By5CAZWd2BixDkp0By5AMCVh2BixDApad2RmwDEl2BixDMiRg2RmwDAlYdmZnwDIk2RmwDMmQgGVnwDIkYNmZnRWC5ZrzZ77S/l07y/wGFFjAApYLWMACFrCABSwXsIAFLGABC1guYAELWMACFrBcwAIWsIAFLGC5gGVIwAIWsIBlSMACFrCABSxguYClkE4YvkK36REIWAKWgAUsAUvAErAkYAlYAhawBCwBS8ASsIAlYAlYErAELAFLwAKWgPXLlyTt5fPN35xnlrbftGcGLGABC1jAAhawgAUsYAELWMACFrCABSxgAQtYwAIWsIAFLGABC1jAAhawgAUsYAELWMACFrCABSxgAQtYwAIWsMb/4M3nbf6OD4T9vzGwgAUsZwAWsIAFLGABC1jAggWwgAUsYDkDsIAFLGABC1jAAhYsgAUsYAHLGYAFLGABC1jAApYxwwJYwAIWsJwBWON/mOZvvJyh/2r+Tx9YwHIGYAELWLAAlu0AC1jAAhawgAUsZwAWsIAFC2DZDrCABSxgAQtYRucMwAIWsGABLNsBFrCcAVjAApbROQOwgFWVb/76/6PxfLM3CSzjAJbnCyxgAQtYNgks4/DMgAUsGQewPF9gGYcXClg2CSzj8MyABSxgGQewPF9gGYcXClg2CSzjAJbnCyxgAQtYni+wxv7gzd+ZNb8k/hzXOcgDC1jAAhawgAUsYAELWMACFrCABSxgAQtYwAIWsIAFLGABC1jAAhawgAUsYAELWMACFrCABSxgAQtYwAIWsIAFLA2H272BcOxv7VUGFrCABSwBC1h+N2DJ8IEFLAELWMACloAFLL8bsGT4wAKWgOXegAUsAQtYfjdgCVjAApaABSxgAetTa63q7/78yaoZYMHNt4TAAhawgAUsF7CABSxgAQtYwAIWsIAFLGABywUsYAELWMACFrCABSxgAQtYwHIBC1jAAhawgAUsYAELWMAC1uFg7b1rX74nz6EZC/8hvAshsIAFLGABC1jAAhawgAUsYAELWMACFrCABSxgAQtYwAIWsIAFLGABC1jAAhawgAUsYAELWMACFrCABSxgAQtYwAJWyAuVBpbv7ZwBWMAClhfVGYAFLGA5A7CABSxgOQOwgAUsWDgDsIAFLGcAFrCABSxnABawgAULYAELWMByBmABC1jAcgZgAQtYsAAWsIAFoYOQP+FPxAELWMACFrCABSxgAQtYwAIWsIAFLGABC1jAAhawgAUsYAELWMACFrCABQtgAQtYwAIWsIAFLGABC1jAAhawgAUsYBWBBc3+ewMWsIDl3oAFLGB5qd0bsIAFLC81sIAFLGC5N2ABC1heavcGLGABy0sNLGABC1juDVjAApaX2r0BC1jA8lIDC1jAApZ7A9axYKU1Aazm7+J88wcsYAELWMACFrCABSwBC1jAAhawgAUsYAELWMACFrAELGABC1jAAhawgAUsYAELWMACFrCABSxgAQtYwAIWsA4Hy9X/ovrzYf3nBRawgAUsYAELWMACFrCABSxgAQtYwAIWsIAFLGABC1jAAhawgAUsYAELWMACFrCABSxgAcsFLGABC1jAAhawgCVJwJIkYEkCliQBS5KAJQlYkgQsScCSJGBJErAkAUuSgCVJwJIELEkCliQBSxKwJAlYkgQsScCSJGBJErAkAUuSgCVJwJIELEkCliQBSxKwJAlYkgQsScCSJGBJErAkhfUHrmEs1vEGqLYAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTktMTAtMzBUMDY6Mjg6NDQrMDA6MDB5tHHMAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE5LTEwLTMwVDA2OjI4OjQ0KzAwOjAwCOnJcAAAACh0RVh0c3ZnOmJhc2UtdXJpAGZpbGU6Ly8vdG1wL21hZ2ljay1mS2IzZXhVTDTceUQAAAAASUVORK5CYII=\" alt=\"\" width=\"100\" height=\"100\" />tesasdad</p>', 'blank.png', '2019-11-04', 1, '2019-11-04 18:56:17', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `feature`
--

CREATE TABLE `feature` (
  `id` int(11) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `url` text,
  `description` text,
  `visible` int(11) DEFAULT '1',
  `icon` varchar(150) DEFAULT NULL,
  `row` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feature_set`
--

CREATE TABLE `feature_set` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `feature` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `gallery_category` int(11) NOT NULL,
  `file` text,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `gallery_category`, `file`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'blank.png', 'Gambar Buku', '2019-10-17', 1, NULL, NULL, 0),
(2, 1, 'blank.png', 'tes', '2019-10-26', 1, NULL, NULL, 0),
(3, 1, 'WhatsApp Image 2019-10-17 at 10.22.52.jpeg', 'tes', '2019-10-26', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_category`
--

CREATE TABLE `gallery_category` (
  `id` int(11) NOT NULL,
  `kategori` varchar(155) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_category`
--

INSERT INTO `gallery_category` (`id`, `kategori`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'UMUM', '2019-10-17', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Coba Keterangan', '2019-10-01', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kontak_wa`
--

CREATE TABLE `kontak_wa` (
  `id` int(11) NOT NULL,
  `nomor` varchar(45) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontak_wa`
--

INSERT INTO `kontak_wa` (`id`, `nomor`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, '6281282000507', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `media_sosial`
--

CREATE TABLE `media_sosial` (
  `id` int(11) NOT NULL,
  `nama` varchar(155) DEFAULT NULL,
  `url` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media_sosial`
--

INSERT INTO `media_sosial` (`id`, `nama`, `url`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Facebook', 'www.facebook.com', NULL, NULL, NULL, NULL, 0),
(2, 'Instagram', 'www.instagram.com', NULL, NULL, NULL, NULL, 0),
(3, 'Twitter', 'www.twitter.com', NULL, NULL, NULL, NULL, 0),
(4, 'Youtube', 'www.youtube.com', NULL, NULL, '2019-11-19 19:13:52', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mission`
--

CREATE TABLE `mission` (
  `id` int(11) NOT NULL,
  `keterangan` text,
  `file` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mission`
--

INSERT INTO `mission` (`id`, `keterangan`, `file`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Coba Keterangan', NULL, '2019-10-01', 1, NULL, NULL, 0),
(2, 'Coba Pak', 'blank.png', '2019-11-26', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `file` text,
  `title` varchar(150) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` longtext,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `file`, `title`, `tanggal`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'IMG-20190715-WA0011.jpg', 'Coba title', '2019-10-01', '<p><img title=\"blank.png\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAYAAAD0eNT6AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAMOAAADDgB22m8jwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABuOSURBVHic7d37j6V3fdjxz7nMzN5vrNeOd20vxRiMAwa8mKQhwUBRgTSoVaGUNKS0atpGSJUq9Uf4FxIpVRVFaZNWSrET0ihKFUUVFIhqIDG2uRVsg1NWi9de2+vZ2V17d25nTn84PGHAu+uZfZ5znsvn9ZJWQrD75eOd4/N9zzOfne2Nx+PolAfe/tHYd+un6x4DgA65cOqX45/+9f11j1GlXqcC4P4Tu6PXezz2HT9W9ygAdMiFk0/FePz6+OjDL9U9SlX6dQ9QsU/GOFz+AFRrcrd8qu4xqtSdJwD3n3hdRHwzojcf+4/XPQ0AXXL+ZESMVyPiTfHRh5+oeZpKdOkJwH+MiPm6hwCgs+Zjctd0QjcC4P4TH4qI99Y9BgCd994f3jmt1/4AuP/E7oj4jbrHACCN3/jh3dNq7Q+AiE9GxC11DwFAGrdEBxYC270E+LeLf5u/9t8LS4AAVGqyBLj5v2n9QmDbnwBY/AOgDq1fCGxvAFj8A6BerV4IbGcAWPwDoBlauxDYzgCw+AdAM7R2IbB9S4BXXPzbzBIgABV7+RLgZq1cCGzjEwCLfwA0SSsXAtsVABb/AGim1i0EticALP4B0GytWghsTwBY/AOg2Vq1ENiOJcBXXPzbzBIgABW79hLgZq1ZCGzLEwCLfwC0QWsWApsfABb/AGiXViwENjsALP4B0E6NXwhsdgBY/AOgnRq/ENjcJcBtLf5tZgkQgIptfQlws0YvBDb5CYDFPwDarNELgc0MAIt/AHRDYxcCmxcAFv8A6JZGLgQ2LwAs/gHQLY1cCGzWEuB1L/5tZgkQgIpd3xLgZo1bCGzaEwCLfwB0UeMWApsTABb/AOi2Ri0ENiMALP4BkENjFgKbEQAW/wDIoTELgfUvAVay+LeZJUAAKlZ+CXCzRiwENuEJgMU/ADJpxEJgvQFg8Q+AnGpfCKwvACz+AZBbrQuBdT4BsPgHQGa1LgTWswRY+eLfZpYAAahYtUuAm9W2EFjXEwCLfwBQ40Lg7APA4h8AbFbLQuBsA8DiHwBcycwXAmf9BMDiHwC83MwXAme3BDjVxb/NLAECULHpLQFuNtOFwFk+AbD4BwBXN9OFwNkEgMU/ANiKmS0ETj8ALP4BwHbMZCFwFk8ALP4BwNbNZCFwukuAM1v828wSIAAVm80S4GZTXwic9hMAi38AsH1TXwicXgBY/AOAMqa6EDidALD4BwBVmNpC4LSeAFj8A4DyprYQWP0SYC2Lf5tZAgSgYrNfAtxsKguB03gCYPEPAKozlYXAagPA4h8ATEPlC4HVBYDFPwCYpkoXAqt8AmDxDwCmp9KFwGqWAGtf/NvMEiAAFat3CXCzyhYCq3oCYPEPAKavsoXA8gFg8Q8AZqmShcByAWDxDwDqUHohsOwTgE+FxT8AmLXSC4HXvwTYqMW/zSwBAlCx5iwBblZqIbDMEwCLfwBQn1ILgdcXABb/AKAJrnshcPsBYPEPAJrkuhYCr+cJgMU/AGiO61oI3N4SYGMX/zazBAhAxZq5BLjZthcCt/sEwOIfADTPthcCtx4AFv8AoMm2tRC4tQCw+AcAbbDlhcCtPgGw+AcAzbflhcBXXgJsxeLfZpYAAahY85cAN9vSQuBWngBY/AOA9tjSQuC1A8DiHwC00SsuBF49ACz+AUCbXXMh8FpPACz+AUB7XXMh8MpLgK1b/NvMEiAAFWvXEuBmV10IvNoTAIt/ANB+V10IfHkAWPwDgC654kLgjweAxT8A6KKXLQT+5BOAT4bFPwDompctBP5oCbDVi3+bWQIEoGLtXQLc7McWAjc/AbD4BwDd9WMLgZMAsPgHABn87UJgb/zpe3ZHxGPRma/9+xIAABXrxpcACj+IiDv74Tv+AUAmt0TEp3rj//t7D0WMt/K3ArbEOOLSs/uu/XN6G9EfvjibeQBotOWze2K0du17cH7fhRlNMxv9wUZv/MQDnXmmAQBsTYc+8wcAtkoAAEBCAgAAEhIAAJCQAACAhAQAACQkAAAgIQEAAAkJAABISAAAQEICAAASEgAAkJAAAICEBAAAJCQAACAhAQAACQkAAEhIAABAQgIAABISAACQkAAAgIQEAAAkJAAAICEBAAAJCQAASEgAAEBCAgAAEhIAAJCQAACAhAQAACQkAAAgIQEAAAkJAABISAAAQEICAAASEgAAkJAAAICEBAAAJCQAACAhAQAACQkAAEhIAABAQgIAABISAACQkAAAgIQEAAAkJAAAICEBAAAJCQAASEgAAEBCAgAAEhrWPQA00vqlM3Hh5JOxen491i4NYrSyEOPRfMS4V/doXEtvHL3BagwWVmJu1yjm9w9j3/HbY7jrprong6bpjZ94YFz3ENAIo5XFOPuNb8ZLT98YG+uvjwiXfTeMoz98PHbf/GwcvvtNMVg4VPdA0AQCAMajS/HcIw/FxVP3RMTeusdhqi7G3lsfiSP33Bu9wa66h4E6CQByu3TmW/H0l26IGHtEnErvTNz8c8/HrpveWPckUBdLgOS1+NiD8fSDr3P5ZzS+KZ5+8HWx+NiDdU8CdREA5PTsQ1+MxW+/IyLm6x6F2szH4rffEc8+9MW6B4E6CADyWXryK3Hx1H11j0FDXDx1Xyw9+ZW6x4BZEwDksnz28Tj79bvrHoOGOfv1u2P57ON1jwGzJABIZDyK0/9nLiJsf/OTdk1eG+NR3YPArAgA8lj8zpdjPHpN3WPQUOPRa2LxO1+uewyYFQFADuPR5Vh8/LV1j0HDLT7+2hiPLtc9BsyCACCHpe896o/78crGN01eK9B9AoAcLnx/UPcItITXCkkIALpvPFqOtZd8xze2Zu2lN8Z4tFz3GDBtAoDuu/Ts4xGxu+4xaI3dP3zNQKcJALpv5fyLdY9Ay3jNkIAAoPvWLq7XPQIt4zVDAgKA7lu75HXO9njNkIAXOd3X6/srr9kerxkSEAB035z9P7bJa4YEBADdN793ru4RaBmvGRIQAHTfwoH9dY9Ay3jNkIAAoPt23nBnRO+FusegLXovTF4z0G0CgAR6/VjY/1jdU9ASC/sfi+h5b6TzvMjJYf/t83WPQEt4rZCEACCHfbediN7gu3WPQcP1Bt+NfbedqHsMmAUBQBK9fhy550LdU9BwR+654PE/WXihk8feW0/EYOFrdY9BQw0WvhZ7b/XZP2kIAHK55e/dHNE7U/cYNE3vzOS1AXkIAHIZ7rwxjr7zXERcrnsUGuNyHH3nuRjuvLHuQWCWBAD57Dx8Zxw58a2IuFT3KNTuUhw58a3Yedif+ycdAUBO+47fG8fuOxXRe6buUahL75k4dt+p2Hf83rongToIAPLacfj1cfwDgxjufLjuUZix4c6H4/gHBrHj8OvrHgXq0hs/8YC/9hJePP21eO6rO2Jj3aPgLusPH4sjb1uOPUffUvcoULdh3QNAI+w5+pbYc3QcL57+Wpz/3sVYfuH2GI9thXdBr/d07HjVk7H/tXtjz9E3R0Sv7pGgCTwBgKtZWXoyVs+fi9WLy7F2cRSjtV7E2OXRaL1xDObGMbd3EPN7d8T8/oOxcOD2uqeCJvIEAK5m4cDtsXCg7ikApsISIAAkJAAAICEBAAAJCQAASEgAAEBCAgAAEhIAAJCQAACAhAQAACQkAAAgIQEAAAkJAABISAAAQEICAAASEgAAkJAAAICEBAAAJCQAACAhAQAACQkAAEhIAABAQgIAABISAACQkAAAgIQEAAAkJAAAICEBAAAJCQAASEgAAEBCAgAAEhIAAJCQAACAhAQAACQkAAAgIQEAAAkJAABISAAAQEICAAASEgAAkJAAAICEBAAAJCQAACAhAQAACQkAAEhIAABAQgIAABISAACQkAAAgIQEAAAkJAAAICEBAAAJCQAASEgAAEBCAgAAEhIAAJCQAACAhAQAACQkAAAgIQEAAAkJAABISAAAQEICAAASEgAAkJAAAICEBAAAJCQAACAhAQAACQkAAEhIAABAQsO6B4BaXLwY8dBXI75/MuLMsxFrqxEHD0Xccizi7jdG3H573RNSpSefjPjGtyJ+8FTEucWIufmIG49E/J1XR9z7toi9e+ueEGauN37igXHdQ8BMfeGLEf/rsxEbG1f/OceORfzav4zYuXNmYzEFly9H/O7vRTz11NV/Tr8f8fffG/Gu+2Y2FjSBACCPy5cj/tNvRzz3/NZ+/mAQ8eEPRbz1zdOdi+l49OsRn/njiNFoaz//yA0Rn/h10UcadgDIYW0t4jd/a+uXf8Tk4vjDP4p44rvTm4vpeOK7k4/dVi//iMlr4zd/a/JagQQEAN23thbx278TsbS0/V87Hkf8/n+LOHmy8rGYkpMnJx+z8XU83FxamrxWRAAJCAC6bW1tchk8dfr6z9jYmHwdWQQ038mTk4/VtfY7XslTpyevGRFAxwkAuqu4/J/8m2rO+s+/LwKa7OTJyceoiov7yb8RAXSeAKCbqrz8C6urIqCpist/dbW6M0UAHScA6J5pXP4FEdA807j8CyKADhMAdMs0L/+CCGiOaV7+BRFARwkAumMWl39BBNRvFpd/QQTQQQKAbpjl5V8QAfWZ5eVfEAF0jACg/eq4/AsiYPbquPwLIoAOEQC0W52Xf0EEzE6dl39BBNARAoD2asLlXxAB09eEy78gAugAAUA7NenyL4iA6WnS5V8QAbScAKB9mnj5F0RA9Zp4+RdEAC0mAGiXJl/+BRFQnSZf/gURQEsJANqjDZd/QQSU14bLvyACaCEBQDu06fIviIDr16bLvyACaBkBQPO18fIviIDta+PlXxABtIgAoNnafPkXRMDWtfnyL4gAWkIA0FxduPwLIuCVdeHyL4gAWkAA0ExduvwLIuDqunT5F0QADScAaJ4uXv4FEfByXbz8CyKABhMANEuXL/+CCPiRLl/+BRFAQwkAmiPD5V8QATku/4IIoIEEAM2Q6fIvZI6ATJd/QQTQMAKA+mW8/AsZIyDj5V8QATSIAKBes7j8+w1/mWeKgLZc/tN8zYgAGqLh74x02iwu//n5iNfeXs1ZVZ1zJRkiYBaXf5Uf6/n5as66EhFAAwgA6jGry/9f/YuIPXuqOe9j/yzijjuqOetKuhwBs7j877hj8jGqwp49k9eOCKDDBACzN8vL//jx6s4cDiM+/jERsF2zuvw//rHJx6gqx4+LADpNADBbbb38CyJge9p6+RdEAB0mAJidtl/+BRGwNW2//AsigI4SAMxGVy7/ggi4tq5c/gURQAcJAKava5d/QQRcWdcu/4IIoGMEANPV1cu/IAJ+XFcv/4IIoEMEANPT9cu/IAImun75F0QAHSEAmI4sl38hewRkufwLIoAOEABUbzyO+INP57n8C7OMgOeem97/x3Ytnov4L/81z+VfmFUE/Pf7p3c+qQkAqve5z0c89vj0zm/i5V+YVQT8waeb85nh//iTiJWV6Z3fxMu/MIsI+M5jEQ9+aXrnk5YAoFqLixGf+9/TO7/Jl39hFhFw5tmIz39xeudv1cOPRHzvyemd3+TLvzCLCPjzv4g4f2F655OSAKBaX/ry5EsA09CGy78wiwh45NHp/V5v1Rf+cnpnt+HyL0w7AkajiK99fTpnk5YAoFrf/NZ0zm3T5V+YdgQsLUWc+sF0zt6KtbWIs2enc3abLv/CtCNAAFAxAUB1Vlen85iyjZd/YdoRcO7cdM7dijNnpvMEoo2Xf2GaEXBusfozSU0AUJ3z56s/s82Xf2GaETDN5btXsjSFj3ebL//CtCJgZYp/yoKUBADV2bu32vO6cPkXphUBdV6UN95Y7XlduPwL04iAubn6dz7oFAFAdXbsiNi5s5qzunT5F6YRAa8+Xt1Z23XD4YiFhWrO6tLlX6g6Am67LaLXq+YsCAFA1d740+XP6OLlX6gyAg7sjzh0qPw516vXi7j1lvLnvK6Dl3+hygh4zavLnwGbCACq9bM/U+7Xd/nyL1QVAe95dzXzlPH+90X0S7yN3HFHxD/v6OVfqCICdu2K+Jm3VzYSRAgAqnb05ut/o8pw+RfKRsCxYxH3vq3ama5rjqMR7/yF6/u1XXzsfzVlI+D975tEAFRIAFC9D/6DiFuObe/X7NiR5/IvFBHwhju39+sOH474lV9uzteD3/ueydent+MNd+a5/AtFBGz3In/H342498RURiI3AUD1hsOIf/NrEW9589Z+/o1HIv7dJ3Jd/oXhMOLjvxrxj//R1hbqjt8W8Yl/G3Ho4PRn26rhMOLX/3XEL35gsql+Lbt2RXzkw5N/5kyXf+H48Yj/8O8jfvquV/65g0HEB94X8cFfak7s0SkJ/w1kJubnIz76kYg3vTHiK381+X7xP/lHmF71qoif+9nJo+xpfh/1Nnj7vZNH4p/9XMSpUxHPn/3R79dgMLn473vnZGGuifr9iHf+fMRdd0Y8+OWIp56KeObM5LsFLixE3HTT5KnQu++L2LOn7mnrtWdPxK/+SsQ3vhnx1Ucmv1eXLv3of9+1K+Ktb4n4hZ+fLHrClAgApuuuN0x+vPhixAuLk29fu2vn5PI/eNBnNpsdPBDxTz40+c8rK5PvtLdrd8SrDpVbtJulw4cj/uEHJ/95YyPi4osR+/b6OF/J3W+a/IiYfEfH8xcmf7Ry9+565yINAcBs7Nkz+XHbrXVP0g4LC9v/unrT9PsR+/fVPUU7HDw4+QEz1JJPKwCAKgkAAEhIAABAQgIAABISAACQkAAAgIQEAAAkJAAAICEBAAAJCQAASEgAAEBCAgAAEhIAAJCQAACAhAQAACQkAAAgIQEAAAkJAABISAAAQEICAAASEgAAkJAAAICEBAAAJCQAACAhAQAACQkAAEhIAABAQgIAABISAACQkAAAgIQEAAAkJAAAICEBAAAJCQAASEgAAEBCAgAAEhIAAJCQAACAhAQAACQkAAAgIQEAAAkJAABISAAAQEICAAASEgAAkJAAAICEBAAAJCQAACAhAQAACQkAAEhIAABAQgIAABISAACQkAAAgIQEAAAkJAAAICEBAAAJCQAASEgAAEBCAgAAEhIAAJCQAACAhAQAACQkAAAgIQEAAAkJAABISAAAQEICAAASEgAAkJAAAICEBAAAJCQAACAhAQAACQkAAEhIAABAQgIAABISAACQkAAAgIQEAAAkJAAAICEBAAAJCQAASEgAAEBCAgAAEhrWPQANsL4e8cwzEU+djnj2uYiNjbonqtapU9Wc86d/FtHXzI1W1Wv31KmIP/nTas5qin4/4sYjEceORvzUT0UMvf1n1xs/8cC47iGoyepqxJ//RcRfP9S9Sx+4un4/4u33Rvzi+yPm5+uehpoIgKy+fzLiDz8TsbhY9yRAXQ4divjIhyNefbzuSaiB55kZnX464nd+1+UP2S0uTt4LTj9d9yTUQABkMxpF/NFnPPIHJjY2Ju8Jo1HdkzBjAiCbz38h4pkzdU8BNMkzZybvDaQiALJ59Ot1TwA0kfeGdARAJsvLvu4PXNni4uQ9gjQEQCann44Y+0MfwBWMx5YBkxEAmVy8WPcEQJN5j0hFAGRy9Oa6JwCazHtEKgIgk8OHIxYW6p4CaKKFhcl7BGkIgEx6vYhbb6l7CqCJbr1l8h5BGgIgmw+8P2IwqHsKoEkGg8l7A6kIgGyO3hzx7nfVPQXQJO9+l6//JyQAMnrPuyLuekPdUwBNcNcbJu8JpONvA8zs4Uci/ux/Riyv1D0JMGs7FiI++EsRJ+6pexJqIgCyWzof8cijEadPRzx1OmJpqe6JgGk5cCDi2NGIo0cj7nlrxIH9dU9EjQQAP251NWKU8G8K3NiI6PuKWApZP9aDfsT8fN1T0CDDugegYbxBAKSQMIMBAAEAAAkJAABISAAAQEICAAASEgAAkJAAAICEBAAAJCQAACAhAQAACQkAAEhIAABAQgIAABISAACQkAAAgIQEAAAkJAAAICEBAAAJCQAASEgAAEBCAgAAEhIAAJCQAACAhAQAACQkAAAgIQEAAAkJAABISAAAQEICAAASEgAAkJAAAICEBAAAJCQAACAhAQAACQkAAEhIAABAQgIAABISAACQkAAAgIQEAAAkJAAAICEBAAAJCQAASEgAAEBCAgAAEhIAAJCQAACAhAQAACQkAAAgIQEAAAkJAABISAAAQEICAAASEgAAkJAAAICEBAAAJCQAACAhAQAACQkAAEhIAABAQgIAABISAACQkAAAgIQEAAAkJAAAICEBAAAJCQAASEgAAEBCwyoO2egvxGhuf+lzhusXojdaLnlKL9YWbig9S3+0HIP1C6XPGc0diI3+fKkzehExXHmu9Cwbg50xGu4tfc5wbSl6G6vlDukPY3nXq0vPMlg7H3MV/N6szx2McX+u1Bm98UYMV8+WnmVjuDtGg92lzxmuLkZvvF7qjHFvGOvzh0rPMhi9FP31l0qfsz5/OMa9cp+39DbWYrh2rvQso+Ge2BjsKn3O3OrZiPFGqTPG/flYnztQepbB+sXojy6XPmd94UiMS57R31iNwdpS6VlGw32xMdhR+py5lecjSv5TjQc7Yn24r/Qsg7Xz0d9YKX1OJQEwmj8Ul/bfXfqcXUuPxlzZAOj149KBt5aeZX756dh5/pulz1nefXusLxwudUZvvB77nvtc6VnW5w/H5X13lT5n97mHYri6WOqMjcGuWNn9mtKzDNcvVhIAy3tfF6OSb6D90eXYe/YvS8+ytnBjLO+5o/Q5e174UgzWL5Y6Y2Ows5J/n3a8+N1YWP9/pc+5vO+u2BjsLHXGYG0p9iz+VelZ1nbeEiu7bit9zr6zX4jeqNyb+Wi4p5KP084L3475yz8ofc6l/W+Kca/c9TJcORu7lx4uPcvq7uOxuuPm0ufsf+6zEeNRqTPWh/sq+TjtOv+N6C8/U/ocXwIAgIQEAAAkJAAAICEBAAAJCQAASEgAAEBCAgAAEhIAAJCQAACAhAQAACQkAAAgIQEAAAkJAABISAAAQEICAAASEgAAkFBv/MQD47KHbPR3xGj+YOlhBquL0d9YKXlKL9Z23FR6lv7ocgzWlkqfsz53MMaDHeUOGW/E3MqzpWfZGOyK0dz+0ucMVxejV/bj1J+P5d2vKT3LYG0p5pafKX3O+vyrYtyfL3fIxnrMrT5fepaN4Z4YDfeWPme48nz0xuulzhj3hrG+cEPpWQbrF6O//mLpc9bmb4joD0ud0dtYjeHqC6VnGQ33xcZwd+lz5laeixiPSp0x7i/E+vyh0rMM1s5Hf3Sp9DlrCzdG9Mp9ftkbLcdw7VzpWUZzB2JjsLP0OXPLZyKi3HW50V+IURUfp9Vz0d9YLn1OJQEAALSLLwEAQEICAAASEgAAkJAAAICEBAAAJCQAACAhAQAACQkAAEhIAABAQgIAABISAACQkAAAgIQEAAAkJAAAICEBAAAJCQAASOj/A/Yy/eb9BanZAAAAAElFTkSuQmCC\" alt=\"\" width=\"100\" height=\"100\" />Keterangan</p>', '2019-10-01', 1, '2019-11-04 19:06:04', 1, 0),
(2, 'blank.png', 'News 2', '2019-11-19', '<p>tes</p>', '2019-11-19', 1, NULL, NULL, 0),
(3, 'blank.png', 'News 3', '2019-11-19', '<p>tes</p>', '2019-11-19', 1, NULL, NULL, 0),
(4, 'blank.png', 'News 4', '2019-11-19', '<p>tes</p>', '2019-11-19', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `our_group`
--

CREATE TABLE `our_group` (
  `id` int(11) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `url` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `our_group`
--

INSERT INTO `our_group` (`id`, `nama`, `url`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Group 1', 'https://solutionsdapps.com', '2019-10-01', 1, '2019-11-04 18:41:11', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `partner`
--

CREATE TABLE `partner` (
  `id` int(11) NOT NULL,
  `file` text,
  `title` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partner`
--

INSERT INTO `partner` (`id`, `file`, `title`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'IMG-20190715-WA0011.jpg', 'Coba Partner', '2019-10-01', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `no_hp` varchar(45) DEFAULT NULL,
  `email` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `alamat`, `no_hp`, `email`, `createddate`, `createdby`, `updateddate`, `updatedby`) VALUES
(1, 'DEFAULT', '-', 'solutionsdapps@gmail.com', NULL, NULL, '2019-11-19 04:43:37', 1);

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id` int(11) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `url` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id`, `nama`, `url`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Program 1', 'http://dapps-solutions.com/manajemen', '2019-10-01', 1, '2019-11-04 18:43:16', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `file` text,
  `header_title` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `file`, `header_title`, `title`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'IMG-20190715-WA0011.jpg', 'Coba Header', 'Title', '2019-10-01', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'admin', 'admin', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wedo`
--

CREATE TABLE `wedo` (
  `id` int(11) NOT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wedo`
--

INSERT INTO `wedo` (`id`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Coba Ketearngan', '2019-10-01', 1, NULL, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`,`user`),
  ADD KEY `fk_announcement_user1_idx` (`user`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`,`user`),
  ADD KEY `fk_event_user1_idx` (`user`);

--
-- Indexes for table `feature`
--
ALTER TABLE `feature`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feature_set`
--
ALTER TABLE `feature_set`
  ADD PRIMARY KEY (`id`,`user`,`feature`),
  ADD KEY `fk_feature_set_feature_idx` (`feature`),
  ADD KEY `fk_feature_set_user1_idx` (`user`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`,`gallery_category`),
  ADD KEY `fk_gallery_gallery_category1_idx` (`gallery_category`);

--
-- Indexes for table `gallery_category`
--
ALTER TABLE `gallery_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kontak_wa`
--
ALTER TABLE `kontak_wa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media_sosial`
--
ALTER TABLE `media_sosial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mission`
--
ALTER TABLE `mission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `our_group`
--
ALTER TABLE `our_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partner`
--
ALTER TABLE `partner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wedo`
--
ALTER TABLE `wedo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `feature`
--
ALTER TABLE `feature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feature_set`
--
ALTER TABLE `feature_set`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gallery_category`
--
ALTER TABLE `gallery_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kontak_wa`
--
ALTER TABLE `kontak_wa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `media_sosial`
--
ALTER TABLE `media_sosial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mission`
--
ALTER TABLE `mission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `our_group`
--
ALTER TABLE `our_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `partner`
--
ALTER TABLE `partner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wedo`
--
ALTER TABLE `wedo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `announcement`
--
ALTER TABLE `announcement`
  ADD CONSTRAINT `fk_announcement_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `fk_event_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `feature_set`
--
ALTER TABLE `feature_set`
  ADD CONSTRAINT `fk_feature_set_feature` FOREIGN KEY (`feature`) REFERENCES `feature` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_feature_set_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `gallery`
--
ALTER TABLE `gallery`
  ADD CONSTRAINT `fk_gallery_gallery_category1` FOREIGN KEY (`gallery_category`) REFERENCES `gallery_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
